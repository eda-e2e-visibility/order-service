package com.streams.eda.samples.order.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Entity
@Table(name = "order-workflow-step-completed")
@Getter
@Setter
public class OrderWorkflowStepCompleted {
    @Id
    @Column(name = "id", nullable = false, updatable = false, insertable = false)
    private int id;

    @Column(name = "key", nullable = false, insertable = false, updatable = false)
    private String orderKey;

    @Column(name = "completionTime", updatable = false, insertable = false)
    private long completionTime;

    @Column(name = "stepName", updatable = false, insertable = false)
    private String stepName;

    @Column(name = "previousWorkflowStep", updatable = false, insertable = false)
    private String previousWorkflowStep;

    public LocalDateTime getCompletionTime() {
        return Instant.ofEpochMilli(completionTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}

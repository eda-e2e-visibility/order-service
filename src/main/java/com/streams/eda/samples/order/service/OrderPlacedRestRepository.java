package com.streams.eda.samples.order.service;

import com.streams.eda.samples.order.core.model.OrderPlaced;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "orders")
public interface OrderPlacedRestRepository extends PagingAndSortingRepository<OrderPlaced, String> {
}
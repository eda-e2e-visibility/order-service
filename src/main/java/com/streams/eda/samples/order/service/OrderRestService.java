package com.streams.eda.samples.order.service;

import com.streams.eda.samples.order.core.OrderService;
import com.streams.eda.samples.order.dto.OrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;
import java.util.UUID;

@RestController
@RequestMapping("orders")
@Slf4j
public class OrderRestService {
    @Autowired
    private OrderService orderService;

    @PostMapping
    public Response create(@RequestBody OrderDto order) {
        return orderService.placeOrder(UUID.randomUUID(), order)
                .fold(e -> Response.serverError().entity(e).build(),
                        v -> Response.accepted().entity(v.getId()).build());
    }
}

package com.streams.eda.samples.order.service;

import com.streams.eda.samples.order.core.model.OrderWorkflowStepCompleted;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "workflow")
public interface OrderWorkflowStepRestRepository extends PagingAndSortingRepository<OrderWorkflowStepCompleted, Integer> {
    List<OrderWorkflowStepCompleted> findByOrderKey(@Param("orderKey") String orderKey);
    OrderWorkflowStepCompleted findFirstByOrderKeyOrderByIdDesc(@Param("orderKey") String orderKey);
}